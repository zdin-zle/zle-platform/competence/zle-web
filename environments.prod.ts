export const Global = {
    production: true,
    API_BASEURL: 'https://dev-zle.offis.de/competencebackend/',
    MEDIA_ENDPOINT: 'https://dev-zle.offis.de/competencebackend/media/',

    // muss bei Änderung im Backend in den Models auch geändert werden
    textFormFieldsLength: 100,
    eventsNewsFormFieldsLength: 150,
    textAreaFormFieldsLength: 4096,

    maxFileSize: 10000000, // 10MB
};
