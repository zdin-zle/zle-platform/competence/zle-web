export const Global = {
    production: false,
    API_BASEURL: 'http://localhost:8000/competencebackend/',
    MEDIA_ENDPOINT: 'http://localhost:8000/media/',

    // muss bei Änderung auch in der environments.prod und im Backend in den Models auch geändert werden
    textFormFieldsLength: 100,
    eventsNewsFormFieldsLength: 150,
    textAreaFormFieldsLength: 4096,

    maxFileSize: 10000000, // 10MB
}
