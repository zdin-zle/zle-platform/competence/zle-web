import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Global } from 'environments';

import { Observable, finalize } from 'rxjs';
import { StepperOrientation } from '@angular/cdk/stepper';

import { Cluster, Profile } from 'src/app/models';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-cluster-form',
  templateUrl: './cluster-form.component.html',
  styleUrls: ['./cluster-form.component.css']
})
export class ClusterFormComponent {
  Global = Global // Expose Global to the template
  stepperOrientation: Observable<StepperOrientation>;
  spinnerVisible: boolean = false;

  id: number;
  cluster: FormGroup
  profileList: Profile[];

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.cluster = this.fb.group({
      id:           [],
      name:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      description:  ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
      profiles:     [[]]
    });
  }

  ngOnInit(): void {
    // get 'id' parameter from the URL, which is provided if the form is used to edit a profile
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.id = params['id'];
        this.api.searchDatabase<Cluster>('cluster/' + this.id).subscribe(data => {
          this.cluster.get('id')!         .setValue(data.id);
          this.cluster.get('name')!       .setValue(data.name);
          this.cluster.get('description')!.setValue(data.description);
          this.cluster.get('profiles')!   .setValue(
            data.members.map(profile => profile.id)
          );
        });
      }
     });

    this.api.searchDatabase('profiles').subscribe(
      (data: any) => this.profileList = data
    );
  }

  selectionChanged(event: any) {
    this.cluster.value.profiles = event;
  }

  submit() {
    this.spinnerVisible = true;
    this.api.submitFormData('post_cluster', this.cluster.value)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe(() => this.router.navigate(['clusters']));
  }
}
