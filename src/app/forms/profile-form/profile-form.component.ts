import { Component } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/cdk/stepper';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, of, finalize } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Global } from 'environments';
import { Cluster, Institut, Lab, Network, Profile, Project, User } from 'src/app/models';
import { ApiService } from 'src/app/api.service';
import { CreateProjectComponent } from '../dialogs/create-project/create-project.component';
import { CreateClusterComponent } from '../dialogs/create-cluster/create-cluster.component';
import { CreateLabComponent } from '../dialogs/create-lab/create-lab.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CreateInstitutComponent } from '../dialogs/create-institut/create-institut.component';

// Custom validator function to check if at least one maintainer and one representative is present in the representatives
export default function atLeastOneMaintainerAndOneRepresentative(control: AbstractControl): { [key: string]: any } | null {
  if(!control.value) return null;
  const representativesArray = control.value;

  if (representativesArray.length < 2) {
    return { invalidPositions: true };
  }

  const hasMaintainer = representativesArray.some((person: any) => person.position === 'maintainer');
  const hasRepresentative = representativesArray.some((person: any) => person.position === 'representative');

  return hasMaintainer && hasRepresentative ? null : { invalidPositions: true };
};

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css'],
})
export class ProfileFormComponent {
  Global = Global // Expose Global to the template
  stepperOrientation: Observable<StepperOrientation>;
  spinnerVisible: boolean = false;

  id: number;

  //Formgroups
  profileForm!: FormGroup;
  representativeForm!: FormGroup;

  //variables that hold the lists for the select and filter components
  projectList: Project[];
  labList: Lab[];
  networkList: Network[];
  clusterList: Cluster[];
  instituteList: Institut[];

  //variables that hold the original ids for the select and filter components
  defaultProjects: number[] = [];
  defaultLabs: number[] = [];
  defaultNetworks: number[] = [];
  defaultClusters: number[] = [];

  // list of blocking file uploads
  uploadInProgress: string[] = [];

  constructor(
    private fb: FormBuilder,
    private breakpointObserver: BreakpointObserver,
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    //determine if stepper is horizontal or vertical
    this.stepperOrientation = this.breakpointObserver.observe(
      '(min-width: 1175px)'
    ).pipe(
      map(({ matches }) => (matches ? 'horizontal' : 'vertical'))
    );

    // just setup formcontrols and validators
    this.initializeForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      // if id is present, the form is in edit mode and all previously set values are loaded from the database
      if (params['id']) {
        this.id = params['id'];

        // load the profile data from db and fill the form
        this.loadProfileData(this.id);
      } else {
        // if not add at least 2 empty people to the form
        this.addPersonToForm('representative');
        this.addPersonToForm('maintainer');
      }
    });

    // load the lists for the select and filter components
    this.loadLists();
  }

  private initializeForm() {
    this.profileForm = this.fb.group({
      // general
      id:                 [],
      title:              ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      shortcut:           ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      organization:       ['',  Validators.maxLength(Global.textFormFieldsLength)],
      institute:          [],
      external_link:      ['',  Validators.maxLength(Global.textFormFieldsLength)],
      logo:               ['',  Validators.required],
      street:             ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      zip:                ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      city:               ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      country:            ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      // representatives and maintainers
      people:             this.fb.array([], atLeastOneMaintainerAndOneRepresentative),
      // researchOverview
      description:        ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
      research_focus:     ['',  Validators.maxLength(Global.textAreaFormFieldsLength)],
      projects:           [[]],
      labs:               [[]],
      // other tabs
      networks:           [[]],
      clusters:           [[]],
      publications_file:  ['']
    });
  }

  private loadProfileData(id: number) {
    this.api.searchDatabase<Profile>(`profile/${id}`).subscribe(data => {

      // Type guard to check if institute is an object with an id
      let instituteId: number | null = null;
      if (data.institute) {
        if (typeof data.institute !== 'number' && 'id' in data.institute) {
          instituteId = data.institute.id;
        }
      }


      this.profileForm.patchValue({
        id:                 data.id,
        title:              data.title,
        shortcut:           data.shortcut,
        organization:       data.organization,
        institute:          instituteId,
        external_link:      data.external_link,
        logo:               data.logo,
        street:             data.address.street,
        zip:                data.address.zip,
        city:               data.address.city,
        country:            data.address.country,
        description:        data.description,
        research_focus:     data.research_focus,
        projects:           data.projects.map ((project: Project) => project.id),
        labs:               data.labs.map     ((lab: Lab)         => lab.id),
        networks:           data.networks?.map((network: Network) => network.id),
        clusters:           data.clusters?.map((cluster: Cluster) => cluster.id),
        publications_file:  data.publications
      });

      data.representatives.forEach(representative => this.addPersonToForm('representative', representative));
      data.maintainers.forEach(maintainer => this.addPersonToForm('maintainer', maintainer));
    });
  }

  private loadLists() {
    this.api.searchDatabase<Project[]> ('projects')  .subscribe(data => this.projectList   = data);
    this.api.searchDatabase<Lab[]>     ('labs')      .subscribe(data => this.labList       = data);
    this.api.searchDatabase<Network[]> ('networks')  .subscribe(data => this.networkList   = data);
    this.api.searchDatabase<Cluster[]> ('clusters')  .subscribe(data => this.clusterList   = data);
    this.api.searchDatabase<Institut[]>('institutes').subscribe(data => this.instituteList = data);
  }

  // display custom error messages for text area fields
  getErrorMessage(field: string, maxLength: number = 0): string {
    const control = this.profileForm.get(field);
    if (control?.hasError('required')) {
      return `${field.replace('_', ' ')} is required.`;
    } else if (control?.hasError('maxlength')) {
      return `${field.replace('_', ' ')} cannot be more than ${maxLength} characters long.`;
    }
    return '';
  }

  // retrieve and save representatives to the form
  get people(): FormArray {
    return this.profileForm.get('people') as FormArray;
  }
  getPeopleFormGroup(index: number) {
    const peop = this.profileForm.controls["people"] as FormArray
    return peop.at(index) as FormGroup;
  }

  // adds person to the form based on given position and the data from the person
  // user data has position field, which is more ore less useless at this point, because position type is determined by the name of the array it comes from (maintainer/representative)
  // example: if a person gets to be both maintainer and representative it is the same person in both arrays (but position field is the same in both arrays -> therefore useless)
  addPersonToForm(
    position: 'representative' | 'maintainer' | null  = null,
    user: User = { id: null, firstname: '', lastname: '', title: '', position: position, email: '', photo: '', orc_id: ''}
  ) {
    const peopleForm = this.fb.group({
      id:         [user.id],
      firstname:  [user.firstname,  Validators.required],
      lastname:   [user.lastname,   Validators.required],
      title:      [user.title],
      email:      [user.email,      Validators.required],
      position:   [position,        Validators.required], // use position from function argument not from user anymore
      orc_id:     [user.orc_id,    [Validators.minLength(19), Validators.maxLength(19)]],
      photo:      [user.photo,      Validators.required]
    });
    const peop = this.profileForm.controls["people"] as FormArray
    peop.push(peopleForm);
  }
  deletePerson(index: number) {
    const peop = this.profileForm.controls["people"] as FormArray
    peop.removeAt(index);
  }

  //handles the dialogs that open, when you want to create new projects, labs or foci/clusters
  private openAndHandleDialog(dialogComponent: any, apiEndpoint: string): Observable<any> {
    const dialogRef = this.dialog.open(dialogComponent, { width: '600px', data: { profileID: this.id } });
    return dialogRef.afterClosed().pipe(
      switchMap(result => {
        if (result) return this.api.searchDatabase(apiEndpoint as string)
        else        return of(null);
      })
    );
  }
  openDialog(apiEndpoint: 'projects' | 'labs' | 'clusters' | 'institute'): void {
    switch (apiEndpoint) {
      case 'projects':
        this.openAndHandleDialog(CreateProjectComponent, apiEndpoint).subscribe(
          data => { if (data) this.projectList = data; }
        );
        break;
      case 'labs':
        this.openAndHandleDialog(CreateLabComponent, apiEndpoint).subscribe(
          data => { if (data) this.labList = data; }
        );
        break;
      case 'clusters':
        this.openAndHandleDialog(CreateClusterComponent, apiEndpoint).subscribe(
          data => { if (data) this.clusterList = data; }
        );
        break;
      case 'institute':
        this.openAndHandleDialog(CreateInstitutComponent, 'institutes').subscribe(
          data => { if (data) this.instituteList = data; }
        );
        break;
    }
  }

  // handle selection changes if new projects, labs, networks or clusters are created and
  // therefore get selected in the profile form
  selectionChanged(event: any, formGroupItem: string) {
    if (formGroupItem === 'institute') {
      this.profileForm.controls[formGroupItem].setValue(event[0]);
      return;
    }
    this.profileForm.value[formGroupItem] = event;
  }

  // block the submit if a file is still in the upload queue
  blockFormSubmit(formGroupItem: string) {
    this.uploadInProgress.push(formGroupItem);
  }
  unblockFormSubmit(formGroupItem: string) {
    this.uploadInProgress = this.uploadInProgress.filter(item => item !== formGroupItem);
  }

  // update fileControl with filename when upload is finished
  updateFile(formGroup: FormGroup, formGroupItem: string, value: string) {
    const fileControl = formGroup.get(formGroupItem);
    if (fileControl) {
      fileControl.setValue(value);
      fileControl.markAsTouched(); // Mark the control as touched to trigger validation
    }
  }

  private hasPendingUploads(): boolean {
    return this.uploadInProgress.length > 0;
  }

  private showUploadInProgressMessage(): void {
    this.snackBar.open(
      `There are still files being uploaded: ${this.uploadInProgress}. Please try your submission again, once the upload finishes.`,
      "close",
      { duration: 5000 }
    );
  }

  submit() {
    if (this.hasPendingUploads()) { this.showUploadInProgressMessage(); return; }
    this.spinnerVisible = true;

    // destructuring the form values into separate variables
    const {
      street,
      zip,
      city,
      country,
      people,
      ...restFormValues
    } = this.profileForm.value;

    // put form values into the correct format back together
    const formValue = {
      ...restFormValues,
      address:         { street, zip, city, country },
      representatives: people.filter((person: any) => person.position === 'representative'),
      maintainers:     people.filter((person: any) => person.position === 'maintainer'),
    };

    // submit
    this.api.submitFormData('post_profile', formValue)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe((response) => {
        this.id = response.id;
        this.router.navigate(['profiles', this.id]);
      });
  }
}
