import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Global } from 'environments';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from 'src/app/api.service';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-upload-box',
  templateUrl: './upload-box.component.html',
  styleUrls: ['./upload-box.component.css']
})
export class UploadBoxComponent {
  @Input() allowedTypes: string[] = [""];
  @Input() description: string;
  @Input() fileClass!: string; // foldername it will be saved at in backend
  @Input() fileLimit: number = 1;
  @Input() defaultFile: string;
  @Input() form: any;
  @Input() controlName: string;

  @Input() required: boolean = true;

  @Output() uploadStarted = new EventEmitter<any>();
  @Output() uploadFinished = new EventEmitter<any>();
  @Output() fileNameReturn = new EventEmitter<any>(); //returned filename from backend

  //for internal component use
  file: File | undefined;
  uploading: boolean = false;

  constructor (
    public snackBar: MatSnackBar,
    private api: ApiService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    //set default files
    if (changes['defaultFile'] ) {
      if (this.defaultFile != "" &&
          this.defaultFile != 'undefined' &&
          this.defaultFile != null
        ) this.file = new File([], this.defaultFile);
    }
  }

  isRequired(control: AbstractControl): boolean {

    if (!control || !control.validator) {
      return false;
    }

    // Extract the list of validators applied to the control
    const validator = control.validator({} as AbstractControl);

    // Check if 'required' is present in the list of validators
    return validator! && validator['required'] !== undefined;
  }

  onFileSelected(event: any) {
    const file = event.target.files![0];
    if (file) this.uploadFile(file);
  }
  uploadFile(file: File) {
    if (file.size > Global.maxFileSize) {
      this.snackBar.open(`File size exceeds ${Global.maxFileSize / 1000000}MB`, "close", {
        duration: 5000,
      });
      return;
    }
    this.uploadStarted.emit();
    this.uploading = true;

    const fileExtension = file.name.split(".").pop()!.toLowerCase();

    if (!this.allowedTypes.includes(fileExtension)) {
      this.snackBar.open(`Only Files in the Format ${this.allowedTypes.join(', ')} are allowed`, "close", {
        duration: 5000,
      });
      return;
    }
    this.file = file; //maybe not needed

    this.api.uploadFile(file, this.fileClass).subscribe({
      next: (response) => {
        this.uploading = false;
        this.uploadFinished.emit();
        this.fileNameReturn.emit(response);
      },
      error: (error) => {
        this.uploading = false;
        this.uploadFinished.emit();
        this.snackBar.open(`Error uploading file: ${error}`, "close", {
          duration: 5000,
        });
      }
    });
  }

  deleteFile() {
    this.fileNameReturn.emit("")
    this.file = undefined;
  }
}
