import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Global } from 'environments';

import { Observable, finalize } from 'rxjs';
import { StepperOrientation } from '@angular/cdk/stepper';

import { map } from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Network, Profile } from 'src/app/models';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-network-form',
  templateUrl: './network-form.component.html',
  styleUrls: ['./network-form.component.css']
})

export class NetworkFormComponent {
  Global = Global // Expose Global to the template
  stepperOrientation: Observable<StepperOrientation>;
  spinnerVisible: boolean = false;

  id: number;

  network: FormGroup;
  profileList: Profile[];
  selectedProfiles: number[];

  // list of blocking file uploads
  uploadInProgress: string[] = [];

  constructor(
    private fb: FormBuilder,
    public api: ApiService,
    breakpointObserver: BreakpointObserver,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.stepperOrientation = breakpointObserver.observe(
      '(min-width: 800px)'
    ).pipe(
      map(({ matches }) => (matches ? 'horizontal' : 'vertical'))
    );

    this.network = this.fb.group({
      id:                   [],
      name:                 ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      shortcut:             ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      external_link:        ['',  Validators.maxLength(Global.textFormFieldsLength)],
      logo:                 ['',  Validators.required],
      description:          ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
      research_description: ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
      street:               ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      zip:                  ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      city:                 ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      country:              ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
      profiles:             [[]]
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      // if id is present, the form is in edit mode and all previously set values are loaded from the database
      if (params['id']) {
        this.id = params['id'];
        this.api.searchDatabase<Network>('network/' + this.id).subscribe(data => {
          this.network.get('id')!.setValue(data.id);
          this.network.get('name')!.setValue(data.name);
          this.network.get('shortcut')!.setValue(data.shortcut);
          this.network.get('external_link')!.setValue(data.external_link);
          this.network.get('logo')!.setValue(data.logo);
          this.network.get('description')!.setValue(data.description);
          this.network.get('research_description')!.setValue(data.research_description);
          this.network.get('street')!.setValue(data.address.street);
          this.network.get('zip')!.setValue(data.address.zip);
          this.network.get('city')!.setValue(data.address.city);
          this.network.get('country')!.setValue(data.address.country);
          this.selectedProfiles = data.members.map(profile => profile.id);
        });
      }
    });

    this.api.searchDatabase<Profile[]>('profiles').subscribe(
      (data: Profile[]) => this.profileList = data
    );
  }

  blockFormSubmit(formGroupItem: string) {
    this.uploadInProgress.push(formGroupItem);
  }
  unblockFormSubmit(formGroupItem: string) {
    this.uploadInProgress = this.uploadInProgress.filter(item => item !== formGroupItem);
  }

  updateLogo(logo: string) {
    const logoControl = this.network.get('logo');
    if (logoControl) {
      logoControl.setValue(logo);
      logoControl.markAsTouched(); // Mark the control as touched to trigger validation
    }
  }

  submit() {
    if (this.uploadInProgress.length > 0) {
      this.snackBar.open(`There are still files being uploaded: ${this.uploadInProgress}. Once the upload finish, please try your submission again.`, "close", {
        duration: 5000,
      });
      return;
    }

    this.spinnerVisible = true;

    const formValue = {
      id: this.network.value.id,
      name: this.network.value.name,
      shortcut: this.network.value.shortcut,
      logo: this.network.value.logo,
      external_link: this.network.value.external_link,
      description: this.network.value.description,
      research_description: this.network.value.research_description,

      address: {
        street: this.network.value.street,
        zip: this.network.value.zip,
        city: this.network.value.city,
        country: this.network.value.country
      },

      profiles: this.selectedProfiles
    };

    this.api.submitFormData('post_network', formValue)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe(
        (response) => {
          this.id = response.id;
          this.router.navigate(['networks', this.id]);
        }
      );
  }
}
