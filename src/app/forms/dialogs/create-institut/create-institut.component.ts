import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';
import { finalize } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-institut',
  templateUrl: './create-institut.component.html',
  styleUrls: ['./create-institut.component.css']
})
export class CreateInstitutComponent {

  Global = Global
  spinnerVisible: boolean = false;

  uploadInProgress: string[] = [];


  //Formbuilder
  formGroup = this.fb.group({
    title:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    shortcut:      ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    description:   ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
    external_link: ['', [Validators.required]],
    logo:          ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]]
  });

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    public dialogRef: MatDialogRef<CreateInstitutComponent>,
    @Inject(MAT_DIALOG_DATA) public returnID: string,
    private snackBar: MatSnackBar,
  ) { }

  save(): void {
    if (this.hasPendingUploads()) { this.showUploadInProgressMessage(); return; }
    this.spinnerVisible = true;
    this.api.submitFormData('post_institute', this.formGroup.value)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe((response) => this.dialogRef.close(response.id));
  }

  // block the submit if a file is still in the upload queue
  blockFormSubmit(formGroupItem: string) {
    this.uploadInProgress.push(formGroupItem);
  }
  unblockFormSubmit(formGroupItem: string) {
    this.uploadInProgress = this.uploadInProgress.filter(item => item !== formGroupItem);
  }

  // update fileControl with filename when upload is finished
  updateFile(formGroup: FormGroup, formGroupItem: string, value: string) {
    const fileControl = formGroup.get(formGroupItem);
    if (fileControl) {
      fileControl.setValue(value);
      fileControl.markAsTouched(); // Mark the control as touched to trigger validation
    }
  }

  private hasPendingUploads(): boolean {
    return this.uploadInProgress.length > 0;
  }

  private showUploadInProgressMessage(): void {
    this.snackBar.open(
      `There are still files being uploaded: ${this.uploadInProgress}. Please try your submission again, once the upload finishes.`,
      "close",
      { duration: 5000 }
    );
  }


}
