import { Component, Inject, Optional } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';
import { finalize } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent {

  Global = Global
  spinnerVisible: boolean = false;
  //Formbuilder
  formGroup = this.fb.group({
    id:           [null],
    name:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    description:  ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]]
  });

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    public dialogRef: MatDialogRef<CreateProjectComponent>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.formGroup.patchValue(data.project);
  }

  save(): void {
    this.spinnerVisible = true;
    this.api.submitFormData('post_project', this.formGroup.value)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe((response) => this.dialogRef.close(response.id));
  }

  deleteFromProfile(): void {
    if (!this.formGroup.value.id) return;
    this.spinnerVisible = true;
    this.api.deleteItemFromProfile('delete_project_from_profile', this.formGroup.value.id , this.data.profileID)
    .pipe(finalize(() => { this.spinnerVisible = false; }))
    .subscribe({
      next: () => {
        this.dialogRef.close(true);
      },
      error: (error) => {
        this.snackBar.open(error.error);
      }
    });
  }

  deleteForever(): void {
    if (!this.formGroup.value.id) return;
    this.spinnerVisible = true;
    this.api.deleteEntry('project', this.formGroup.value.id, this.formGroup.value.name!, true)
  }
}
