import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'app-create-cluster',
  templateUrl: './create-cluster.component.html',
  styleUrls: ['./create-cluster.component.css']
})
export class CreateClusterComponent {
  Global = Global
  spinnerVisible: boolean = false;
  //Formbuilder
  formGroup = this.fb.group({
    name:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    description:  ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]]
  });

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    public dialogRef: MatDialogRef<CreateClusterComponent>,
    @Inject(MAT_DIALOG_DATA) public returnID: string
  ) { }

  save(): void {
    this.spinnerVisible = true;
    this.api.submitFormData('post_cluster', this.formGroup.value)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe((response) => this.dialogRef.close(response.id));
  }
}
