import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Global } from 'environments';

import { DatePipe } from '@angular/common';
import { DateAdapter } from '@angular/material/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { ApiService } from 'src/app/api.service';
import { finalize } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'de-DE' }
  ],
})
export class CreateEventComponent {
  Global = Global
  spinnerVisible: boolean = false;
  //Formbuilder
  formGroup = this.fb.group({
    networkID:    ['', Validators.required],
    name:         ['', [Validators.required, Validators.maxLength(Global.eventsNewsFormFieldsLength)]],
    description:  ['', [Validators.required, Validators.maxLength(Global.textAreaFormFieldsLength)]],
    date:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    street:       ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    zip:          ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    city:         ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    country:      ['', [Validators.required, Validators.maxLength(Global.textFormFieldsLength)]],
    photo:        ['']
  });

  // list of blocking file uploads
  uploadInProgress: string[] = [];

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    public dialogRef: MatDialogRef<CreateEventComponent>,
    private datePipe: DatePipe,
    private _adapter: DateAdapter<any>,
    @Inject(MAT_DIALOG_DATA) public data: { networkID: string },
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    private snackBar: MatSnackBar
  ) {
    this.formGroup.get('networkID')!.setValue(this.data.networkID);
    this._adapter.setLocale(this._locale);
  }

  blockFormSubmit(formGroupItem: string) {
    this.uploadInProgress.push(formGroupItem);
  }
  unblockFormSubmit(formGroupItem: string) {
    this.uploadInProgress = this.uploadInProgress.filter(item => item !== formGroupItem);
  }

  save(): void {
    if (this.uploadInProgress.length > 0) {
      this.snackBar.open(`There are still files being uploaded: ${this.uploadInProgress}. Once the upload finishes, try your submission again.`, "close", {
        duration: 5000,
      });
      return;
    }

    this.spinnerVisible = true;

    const selectedDate = this.formGroup.get('date')!.value;
    const formattedDate = this.datePipe.transform(selectedDate, 'yyyy-MM-dd HH:mm:ss');
    const formValue: any = {
      networkID: this.formGroup.value.networkID,
      name: this.formGroup.value.name,
      description: this.formGroup.value.description,
      photo: this.formGroup.value.photo,
      address: {
        street: this.formGroup.value.street,
        zip: this.formGroup.value.zip,
        city: this.formGroup.value.city,
        country: this.formGroup.value.country
      },
      date: formattedDate
    };

    this.api.submitFormData('post_event', formValue)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe(() => this.dialogRef.close(true));
  }

  //uploading files
  updateFile(formGroup: FormGroup, formGroupItem: string, value: string) {
    const fileControl = formGroup.get(formGroupItem);
    if (fileControl) {
      fileControl.setValue(value);
      fileControl.markAsTouched(); // Mark the control as touched to trigger validation
    }
  }
}
