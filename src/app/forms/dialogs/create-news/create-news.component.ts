import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';
import { finalize } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.css']
})
export class CreateNewsComponent {
  Global = Global
  spinnerVisible: boolean = false;
  //Formbuilder
  formGroup = this.fb.group({
    title:        ['', [Validators.maxLength(Global.eventsNewsFormFieldsLength)]],
    description:  ['', [Validators.maxLength(Global.textAreaFormFieldsLength)]],
    photo:        ['']
  });

  // list of blocking file uploads
  uploadInProgress: string[] = [];

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    public dialogRef: MatDialogRef<CreateNewsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { networkID: string },
    private snackBar: MatSnackBar
  ) { }

  blockFormSubmit(formGroupItem: string) {
    this.uploadInProgress.push(formGroupItem);
  }
  unblockFormSubmit(formGroupItem: string) {
    this.uploadInProgress = this.uploadInProgress.filter(item => item !== formGroupItem);
  }

  save(): void {
    if (this.uploadInProgress.length > 0) {
      this.snackBar.open(`There are still files being uploaded: ${this.uploadInProgress}. Once the upload finish, please try your submission again.`, "close", {
        duration: 5000,
      });
      return;
    }

    this.spinnerVisible = true;

    const formValue: any = {
      networkID: this.data.networkID,
      ...this.formGroup.value
    }

    this.api.submitFormData('post_news', formValue)
      .pipe(finalize(() => {
        this.spinnerVisible = false;
      }))
      .subscribe(() => this.dialogRef.close(true));
  }

  //uploading files
  updateFile(formGroup: FormGroup, formGroupItem: string, value: string) {
    const fileControl = formGroup.get(formGroupItem);
    if (fileControl) {
      fileControl.setValue(value);
      fileControl.markAsTouched();
    }
  }
}
