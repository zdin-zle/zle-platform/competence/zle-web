import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';

// directive to handle dragover events for the file dropper
// it emits the files to the parent component
@Directive({
  selector: '[appDragover]'
})
export class DragoverDirective {

  @Output() private filesChangeEmiter: EventEmitter<File[]> = new EventEmitter();

  @HostBinding('style.background') private background: any;
  @HostBinding('style.border') private borderStyle: any;
  @HostBinding('style.border-color') private borderColor: any;
  @HostBinding('style.border-radius') private borderRadius: any;

  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.borderColor = 'cadetblue';
    this.borderStyle = '3px dashed';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.borderStyle = '2px solid #ccc';
  }


  @HostListener('drop', ['$event']) public onDrop(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.borderStyle = '2px solid #ccc';

    let files = evt.dataTransfer.files;
    this.filesChangeEmiter.emit(Array.from(files));
  }
}
