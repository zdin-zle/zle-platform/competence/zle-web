import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationEnd, Router } from '@angular/router';
import { Global } from 'environments';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CloseDialogComponent } from './components/close-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
  This service is used to make API Calls to the backend.
  - Single items can be retrieved
  - all items, or filtered by a search string, can be retrieved from one type
  - form data in form of a single item can be submitted
  - news/events can be deleted from a profile
  - files can be uploaded
*/
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  /**
   * This function forwards the search input to the backend and returns the search result as an observable of type T that is specified when calling the function.
   * @param {string} endpoint - The endpoint to search in provided by the backend. (can be found throug swagger documentation)
   * @param {string} searchInput - The search input string to search for.
   * @returns {Observable<T>} - The search result as an observable.
   */
  searchDatabase<T>(endpoint: string, searchInput: string = ''): Observable<T> {
    const formattedSearchInput = searchInput ? `${searchInput}` : '';
    const url = `${Global.API_BASEURL}${endpoint}?search=${formattedSearchInput}&format=json`;

    return this.http.get<T>(url);
  }

  /**
   * This function is used to get a single item from the backend.
   * @param {string} endpoint - The endpoint to get the item from provided by the backend. (can be found throug swagger documentation)
   * @param {number} id - The id of the item to get.
   * @returns {Observable<T>} - The item as an observable.
   * @template T - The type of the item to get.
   *
   * @example
   * getSingleItem<Profile>('profiles', 1).subscribe(profile => console.log(profile));
   * // This will log the profile with the id of 1 to the console.
   */
  getSingleItem<T>(endpoint: string, id: number): Observable<T> {
    // currently only used for clusters on the map
    const url = `${Global.API_BASEURL}${endpoint}/${id}`;
    return this.http.get<T>(url);
  }

  /**
   * This function handles the deletion api call of any items. It opens a dialog beforehand to confirm the deletion by the user.
   * @param {string} type - The type of the item to be deleted.
   * @param {number} id - id of the item
   * @param {string} title - The title of the item to show in the deletion confirm dialog.
   * @param {boolean} shouldReload - If the page should reload after deletion or not. Default is false.
   */
  deleteEntry(
    type: 'profile' | 'network' | 'cluster' | 'project' | 'lab' | 'event' | 'news' | 'institute',
    id: number,
    title: string,
    shouldReload = false
  ): void {
    const dialogRef = this.dialog.open(CloseDialogComponent, { width: '500px', data: { type, title } });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;

      const endpoints: any = {
        profile:   { del: 'delete_profile',   list: 'profiles'   },
        network:   { del: 'delete_network',   list: 'networks'   },
        cluster:   { del: 'delete_cluster',   list: 'clusters'   },
        project:   { del: 'delete_project',   list: 'projects'   },
        lab:       { del: 'delete_lab',       list: 'labs'       },
        event:     { del: 'delete_event',     list: null         },
        news:      { del: 'delete_news',      list: null         },
        institute: { del: 'delete_institute', list: 'profiles' },
      };

      // assign the actual delEndpoint and listEndpoint based on the given endpoint type
      const { del: delEndpoint, list: listEndpoint } = endpoints[type];

      const reloadPage = () => {
        if (shouldReload) {
          window.location.reload();
        } else if (listEndpoint) {
          this.router.navigate([`/${listEndpoint}`]);
        }
      };

      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd && event.urlAfterRedirects === `/${listEndpoint}`) {
          reloadPage();
        }
      });

      const url = `${Global.API_BASEURL}${delEndpoint}/${id}/`;
      this.http.delete(url).subscribe(reloadPage);
    });
  }

  // provides the cookie to retrieve the CSRF token
  // to retrieve the CSRF token from the cookie call this function with 'csrftoken'
  private getCookie(key: string): string {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${key}=`);
    if (parts.length === 2) return parts.pop()!.split(';').shift()!
    return '';
  }

  // creates error message from backend error response
  private serializeError(error: any): string {
    if (typeof error === 'string') {
      return error;
    }
    if (error.error) {
      return this.serializeError(error.error);
    }
    if (typeof error === 'object' && error !== null) {
      return JSON.stringify(error, null, 2);
    }
    return 'An unknown error occurred';
  }

  /**
   * This function is used to submit form data to the backend.
   * it opens a snackbar to display the error message if the backend returns an error
   * otherwise it returns the response. Which is the created object (currently not used by frontend)
   * @param {string} endpoint - The endpoint to submit the form data to.
   * @param {object} formObject - The form data object to submit.
   * @returns {Observable<any>} - The response from the backend as an observable.
   */
  submitFormData(endpoint: string, formObject: object): Observable<any> {
    // Create the request headers with the CSRF token
    const headers = new HttpHeaders({
      'X-CSRFTOKEN': this.getCookie('csrftoken')
    });

    // return response but intercept error
    return this.http.post(Global.API_BASEURL +  endpoint + '/', formObject, { headers }).pipe(tap({
      error: (error) => {
        const errorMessage = this.serializeError(error);
        this.snackBar.open(`${errorMessage }`, "Close", {
          duration: 30000,
        });
      }
    }));
  }

  /**
   * This function is used to upload a file to the backend.
   * It can determine in which folder the file will be saved in the backend.
   * @param {File} file - The file to upload.
   * @param {string} fileClass - the classification of the file. It determines in which folder the file will be saved in the backend.
   *    currently: "publications" > "upload_bib"
   *                all other classes are images therefore > "upload_image"
   * todo: discard this functionality and send the file data as file fields alongside the form data in the general submit
   * @returns {Observable<any>} - The response from the backend as an observable.
   * @example
   * (file, 'publications').subscribe(response => console.log(response));
   * // This will log the response from the backend to the console.
   */

  uploadFile(file: File, fileClass: string): Observable<any> {

    const formData = new FormData();
    //only single file upload works for now
    formData.append(fileClass, file);

    // Create the request headers with the CSRF token
    const headers = new HttpHeaders({
      'X-CSRFTOKEN': this.getCookie('csrftoken')!
    });

    //set upload endpoint
    var endpoint = Global.API_BASEURL + (fileClass == "publications" ? 'upload_bib/' : 'upload_image/');
    return this.http.post(endpoint, formData, { headers });
  }

  /**
   * This function is used to delete an item (news/events) from an existing profile.
   * @param {string} endpoint - The endpoint to delete the item from.
   * @param {number} itemID - The id of the item to delete.
   * @param {number} profileID - The id of the profile to delete the item from.
   * @returns {Observable<any>} - The response from the backend as an observable.
   */
  deleteItemFromProfile(endpoint: string, itemID: number, profileID: number): Observable<any> {
    const url = `${Global.API_BASEURL}${endpoint}/`;
    const body = { profileID, itemID };

    return this.http.put<any>(url, body);
  }
}
