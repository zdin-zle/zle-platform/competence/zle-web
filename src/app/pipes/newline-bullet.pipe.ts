import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'newlineBullet'
})
export class NewlineBulletPipe implements PipeTransform {

  // makes bulletpoint list from newlines "\n"
  transform(value: string): string {
    return "<ul><li>" + value.replaceAll("\n", "</li><li>") + "</li></ul>";
  }
}