import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'newline'
})
export class NewlinePipe implements PipeTransform {

  // transforms newlines to <br> tags
  transform(value: string): string {
    return value.replaceAll("\n", "<br>");
  }
}
