import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'close-dialog',
  template: `
    <h1 mat-dialog-title>Do you really want to delete the {{data.type}} {{data.title}}?</h1>
    <div mat-dialog-actions>
      <button mat-button (click)="this.dialogRef.close(true)">Yes</button>
      <button mat-button mat-dialog-close>No</button>
    </div>
  `,
})
export class CloseDialogComponent {
  constructor(public dialogRef: MatDialogRef<CloseDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
}
