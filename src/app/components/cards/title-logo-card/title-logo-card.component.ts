import { Component, Input } from '@angular/core';
import { Global } from 'environments';
import { Network, Profile } from 'src/app/models';

@Component({
  selector: 'app-title-logo-card',
  templateUrl: './title-logo-card.component.html',
  styleUrls: ['./title-logo-card.component.css']
})
export class TitleLogoCardComponent {
  @Input() link: string;
  @Input() title: string;
  @Input() shortcut: string;
  @Input() logo: string;

  logoPath: string;

  ngOnInit(): void {
    this.logoPath = Global.MEDIA_ENDPOINT + 'logos/' + this.logo;
  } 
}
