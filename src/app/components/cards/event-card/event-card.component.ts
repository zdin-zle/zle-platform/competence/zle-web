import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EventCardDialogComponent } from './event-card-dialog/event-card-dialog.component';
import { Global } from 'environments';
import { NetworkEvent } from 'src/app/models';


@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.css']
})
export class EventCardComponent {
  @Input() event: NetworkEvent;

  mediaEndpoint: string;
  date: string;

  constructor(
    public dialog: MatDialog
  ) { 
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  ngOnInit() {
    let formattedDate = this.event.date.split('T')[0];
    formattedDate = formattedDate.split('-').reverse().join('.');
    this.event.date = formattedDate;
  }

  openDialog() {
    const dialogRef = this.dialog.open(EventCardDialogComponent, { 
      width: 'object-fit', 
      data: { event: this.event }
    });
  }
}
