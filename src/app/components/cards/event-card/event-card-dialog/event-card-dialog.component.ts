import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Global } from 'environments';
import { NetworkEvent } from 'src/app/models';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-event-card-dialog',
  templateUrl: './event-card-dialog.component.html',
  styleUrls: ['./event-card-dialog.component.css']
})
export class EventCardDialogComponent {

  event: NetworkEvent;
  mediaEndpoint: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { event: NetworkEvent },
    public http: HttpClient,
    public router: Router,
    public dialog: MatDialog,
    public api: ApiService
  ) {
    this.event = data.event;
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }
}
