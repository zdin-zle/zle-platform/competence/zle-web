import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input } from '@angular/core';
import { Global } from 'environments';
import { Cluster } from 'src/app/models';

@Component({
  selector: 'app-cluster-card',
  templateUrl: './cluster-card.component.html',
  styleUrls: ['./cluster-card.component.css']
})

export class ClusterCardComponent {
  @Input() id: number;
  @Input() title: string;
  @Input() description: string;
  @Input() logos: string[];
  @Input() cluster: Cluster;

  mediaEndpoint: string;
  showMore: boolean = false;
  constructor(
    private elementRef: ElementRef) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  ngOnInit() {
    this.elementRef.nativeElement.style.setProperty('--rowNum', ""+ this.cluster.members.length);
  }
}
