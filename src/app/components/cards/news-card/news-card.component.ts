import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent {
  @Input() id: number;
  @Input() title: string;
  @Input() created: string;
  @Input() photo: string;
  @Input() description: string;

  isFullImage: boolean = false;
  expandable: boolean = false; // if image is expandable -> cursor pointer

  constructor(
    public http: HttpClient,
    public router: Router,
    public dialog: MatDialog,
    public api: ApiService
  ) {}

  photoPath: string;
  ngOnInit(): void {
    this.photoPath = Global.MEDIA_ENDPOINT + 'news/' + this.photo;
  }

  toggleFullImage() {
    this.isFullImage = !this.isFullImage;
  }

  onImageLoad(imgElement: HTMLImageElement) {
    this.expandable = imgElement.clientHeight == 300;
  }
}
