import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-select-and-filter',
  templateUrl: './select-and-filter.component.html',
  styleUrls: ['./select-and-filter.component.css']
})
export class SelectAndFilterComponent {
  // This component handles a selectable list that can be filtered by a search input

  @Input() list: any[];
  @Input() label: string;
  @Input() placeholder: string;
  @Input() defaultSelection: number[] = [];
  @Input() multiple: boolean = true;

  @Output() selectedListChanged = new EventEmitter<number[]>();

  // list of objects
  listFiltered: any[];

  // list of ids
  selectionModel: number[] = [];

  // search string
  filterModel: string = '';

  // for determination if the items name is 'title' or 'name'
  titleProperty: string = '';

  // react to added items
  ngOnChanges(changes: SimpleChanges): void {

    if (changes['list']) {

      //reset the filter and the listFiltered
      this.filterModel = '';
      this.listFiltered = this.list;

      // needed because no consistent naming conventions
      if (this.list.length > 0) {
        if ('title' in this.list[0]) this.titleProperty = 'title';
        else if ('name' in this.list[0]) this.titleProperty = 'name';
      }

      // necessary in profile form:
      //  if an item is added to the list via the new created button,
      //  add it automatically to the selectionModel
      const prevList = changes['list'].previousValue;
      if (prevList) {

        // find out the added item
        if (prevList.length == this.list.length - 1) {
          const addedItem = this.list.find((item: any) => !prevList.includes(item));

          if (this.selectionModel instanceof Array) {
            this.selectionModel.push(addedItem.id);
          } else this.selectionModel = [addedItem.id];

          this.selectedListChanged.emit(this.selectionModel);
        }
      }
    }

    if (changes['defaultSelection']) {
      this.selectionModel = this.defaultSelection;
    }
  }

  public filterList() {
    const filterValue = this.filterModel.toLowerCase();
    this.listFiltered = this.list.filter(
      item => item[this.titleProperty].toLowerCase().includes(filterValue)
    );
  }

  // Emit the selectedListChanged event when the selection changes
  public onSelectionChange(): void {
    // if is array, then filter the selected items based on the listFiltered
    if (this.selectionModel instanceof Array) {
      this.selectionModel = this.selectionModel.filter(
        selectedId => this.listFiltered.some(item => item.id === selectedId)
      );
    } else {
      this.selectionModel = [this.selectionModel];
    }
    this.selectedListChanged.emit(this.selectionModel);
  }
}
