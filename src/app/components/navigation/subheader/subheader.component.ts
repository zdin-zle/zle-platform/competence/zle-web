import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.css']
})
export class SubheaderComponent {
  parentRoute: string;
  currentRoute: string;

  routerEventsSubscription: Subscription;

  constructor(private router: Router ) {
    this.currentRoute = this.router.url.split('/')[1];
    this.parentRoute =  this.calculateParentRoute();
  }

  ngOnInit(): void {
    this.routerEventsSubscription = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.parentRoute =  this.calculateParentRoute();
      });

    // Initial calculation
    this.parentRoute =  this.calculateParentRoute();
  }

  ngOnDestroy(): void {
    if (this.routerEventsSubscription) {
      this.routerEventsSubscription.unsubscribe();
    }
  }


  calculateParentRoute() {
    const urlSegments = this.router.url.split('/');
    if (urlSegments.length <= 1) return '/';

    // take out 'edit' if it is present
    if (urlSegments.includes('edit')) {
      return urlSegments.filter(segment => segment !== 'edit').join('/');
    }

    // else remove last element
    urlSegments.pop();
    return urlSegments.join('/');
  }

}
