export interface Network {
  id: number;
  name: string;
  shortcut: string;
  logo: string;
  events: [];
  news: [];
  description: string;
  research_description: string;
  members: Profile[];
  address: Address;
  external_link: string;
}

export interface User {
  id: number | null;
  firstname: string;
  lastname: string;
  title: string;
  position: 'representative' | 'maintainer' | null;
  email: string;
  photo: string;
  orc_id: string;
}

export interface Profile {
  id: number;
  title: string;
  organization: string;
  institute: Institut | null;
  shortcut: string;
  author: number;
  description: string;
  logo: string;
  research_focus: string;
  representatives: [];
  maintainers: [];
  publications: string;
  publications_file: string;
  external_link: string;
  address_id: number;
  author_id: number;
  address: Address;
  projects: [];
  labs: [];
  networks?: Network[];
  clusters?: Cluster[];
}

export interface Cluster {
  id: number;
  name: string;
  description: string;
  members: Profile[];
}

export interface Project {
  id: number;
  name: string;
  description: string;
}

export interface Lab {
  id: number;
  name: string;
  description: string;
}

export interface Address {
  id: number;
  street: string;
  zip: string;
  city: string;
  state: string;
  country: string;
  latitude: number;
  longitude: number;
}

export interface Publication {
  type: string;
  id: string;
  citekey: string;
  collection: string;
  title: string;
  year: string;
  author: { name: string }[];
  journal?: { name: string; volume?: string; number?: string };
  booktitle?: string;
  pages?: string;
  publisher?: { name: string; address?: string };
}

export interface selectedList {
  key: number,
  value: string,
  optionalImage?: string,
  selected: boolean,
  color?: number
}

export interface NetworkEvent {
  id: number;
  name: string;
  description: string;
  date: string;
  photo: string;
  address: Address;
  networkID: Network;
}

export interface ProfileMap {
  type: string;
  id: number;
  title: string;
  shortcut: string;
  logo: string;
  address: Address;
  color?: number;
}

export interface NetworkMap {
  type: string;
  id: number;
  name: string;
  shortcut: string;
  logo: string;
  address: Address;
  members: number[];
}

export interface ClusterMap {
  type: string;
  id: number;
  name: string;
  description: string;
  members: number[];
}

export interface MarkerBase {
  id: number,
  type: string,
}

export interface Marker {
  id: number,
  type: string,
  address: Address
  color?: number,
  icon?: string,
}


export interface Institut {
  id: number;
  title: string;
  shortcut: string;
  description: string;
  external_link: string;
  logo: string;
  members: number[];
}
