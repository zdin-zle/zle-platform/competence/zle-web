import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './content-views/home/home.component';
import { NetworkComponent } from './content-views/network/network.component';
import { ProfileComponent } from './content-views/profile/profile.component';
import { ProfileFormComponent } from './forms/profile-form/profile-form.component';
import { MapComponent } from './content-views/map/map.component';
import { MainListPanelComponent } from './content-views/main-list-panel/main-list-panel.component';
import { NetworkFormComponent } from './forms/network-form/network-form.component';
import { ClusterFormComponent } from './forms/cluster-form/cluster-form.component';
import { PagenotfoundComponent } from './content-views/pagenotfound.component';
import { ProfileListComponent } from './content-views/profile-list/profile-list.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'profiles', component: ProfileListComponent,
    data: {
      endpoint: 'profiles',
      hasNewButton: true,
      searchText: 'Search Competence Profiles',
      newButtonText: 'New Profile',
      description: `
A competence <b>profile</b> displays a variety of information about the profile owner including the research focus, research projects, available facilities, i.e., laboratories, and current publications. Other than that, the participation in research clusters and research networks is visible. The profiles are created on the basis of a form-based approach. <br><br>
An <b>institution</b> is the overlaying organization of a profile, e.g., the corresponding university of a competence profile of a university department. Hence, multiple profiles can belong to an institution.
      `
    }
  },
  {
    path: 'networks', component: MainListPanelComponent,
    data: {
      endpoint: 'networks',
      hasNewButton: true,
      searchText: 'Search Research Networks',
      newButtonText: 'New Network',
      description: 'Research networks are pre-established collaborative associations that institutions are already part of. These networks serve as foundational structures within the research ecosystem, bringing together institutions and researchers with common affiliations or broader research goals. They facilitate knowledge exchange, resource sharing, and collaborative endeavours, enabling institutions to leverage their connections and expertise for collective research advancement.'
    }
  },
  {
    path: 'clusters', component: MainListPanelComponent,
    data: {
      endpoint: 'clusters',
      hasNewButton: true,
      searchText: 'Search Research Cluster',
      newButtonText: 'New Cluster',
      description: 'Research Clusters allow the grouping of competence based on a joint research focus. The clustering helps synthesize groups of researchers who may not belong to the same institution or project but still focus on the same research topic. Moreover, it also provides a connecting point for new researchers.'
    }
  },
  { path: 'map', component: MapComponent },
  { path: 'profiles/new', component: ProfileFormComponent },
  { path: 'networks/new', component: NetworkFormComponent },
  { path: 'clusters/new', component: ClusterFormComponent },
  { path: 'profiles/:id', component: ProfileComponent },
  { path: 'networks/:id', component: NetworkComponent },
  { path: 'profiles/edit/:id', component: ProfileFormComponent },
  { path: 'networks/edit/:id', component: NetworkFormComponent },
  { path: 'clusters/edit/:id', component: ClusterFormComponent },
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
