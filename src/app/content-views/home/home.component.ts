import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  tiles = [
    {
      link: '/profiles',
      image: 'assets/landingpage/profile.svg',
      title: 'Profiles',
      description: "Get easy and multi-layered access to all relevant information, skills, research projects, and literature references to demonstrate the profile's competence, including semi-automated updating."
    },
    {
      link: '/networks',
      image: 'assets/landingpage/network.svg',
      title: 'Research Networks',
      description: "Get access to collaborative structures formed by institutions with shared research interests and objectives by research networks, which facilitate communication, collaboration, and the exchange of information among researchers working on similar topics."
    },
    {
      link: '/clusters',
      image: 'assets/landingpage/cluster.png',
      title: 'Research Clusters',
      description: "Get an overview of existing common research topics independent of projects and institutions and a connecting point for new researchers from the research clusters that emerge from the research profiles."
    },
    {
      link: '/map',
      image: 'assets/landingpage/map.svg',
      title: 'Research Map',
      description: "Get an interactive display of all registered profiles on a research map, with sorting and filter functions to show the geographical spread of research clusters and networks."
    }
  ];

  router: Router;
  constructor(router: Router) {
    this.router = router;
  }
}
