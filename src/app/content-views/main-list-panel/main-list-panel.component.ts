import { Component } from '@angular/core'
import { Global } from 'environments';
import { Observable } from 'rxjs';
import { Cluster } from '../../models';
import { ActivatedRoute } from '@angular/router';

import { map } from 'rxjs/operators';
import { ClusterDialogComponent } from 'src/app/content-views/cluster-dialog/cluster-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-main-list-panel',
  templateUrl: './main-list-panel.component.html',
  styleUrls: ['./main-list-panel.component.css']
})
export class MainListPanelComponent {
  endpoint: string;
  hasNewButton: boolean = false;
  searchText: string = '';
  newButtonText: string = '';
  description: string = '';

  mediaEndpoint: string;
  list$: Observable<any[]>;
  tempList$: Observable<any[]>;

  searchInput: string;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.endpoint = data['endpoint'];
      this.hasNewButton = data['hasNewButton'];
      this.searchText = data['searchText'];
      this.newButtonText = data['newButtonText'];
      this.description = data['description'];
    });
    this.list$ = this.api.searchDatabase(this.endpoint);
    this.tempList$ = this.list$;
  }

  search() {
    const searchInput = this.searchInput ? this.searchInput.trim().toLowerCase() : '';

    //filter out items that don't match search input in list observable
    this.tempList$ = this.list$.pipe(
      map(items => items.filter(item => {
        if (this.endpoint == 'profiles') {
          return item.title.toLowerCase().includes(searchInput) || item.shortcut?.toLowerCase().includes(searchInput);
        }
        return item.name.toLowerCase().includes(searchInput) || item.shortcut?.toLowerCase().includes(searchInput);
      }))
    );
  }

  openClusterDialog(cluster: Cluster) {
    const dialogRef = this.dialog.open(ClusterDialogComponent, { width: '500px' });
    dialogRef.componentInstance.cluster = cluster;
  }
}
