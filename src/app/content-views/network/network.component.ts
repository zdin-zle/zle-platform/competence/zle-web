import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from 'environments';
import { Network } from 'src/app/models';
import { MatDialog } from '@angular/material/dialog';
import { CreateEventComponent } from 'src/app/forms/dialogs/create-event/create-event.component';
import { CreateNewsComponent } from 'src/app/forms/dialogs/create-news/create-news.component';
import { ApiService } from 'src/app/api.service';
import { Utils } from 'src/app/utils';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent {
  public utils = Utils;

  researchNetwork: Network;

  id: number;
  logoPath: string;

  constructor(
    public http: HttpClient,
    public router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public api: ApiService,
    public sanitizer: DomSanitizer,
  ) {
    //get last element of routes array
    this.id = parseInt(route.snapshot.url.slice(-1)[0].path)
  }

  ngOnInit() {
    // this.researchNetwork$ = this.http.get<Network>(Global.API_BASEURL + 'network/' + this.id + '?format=json');
    this.api.searchDatabase<Network>('network/' + this.id).subscribe(
      network => {
        this.researchNetwork = network;
        this.logoPath = Global.MEDIA_ENDPOINT + 'logos/' + network.logo;
      }
    );
  }

  // dialog functions that are called from the template
  openDialogNewEvent(): void {
    const dialogRef = this.dialog.open(CreateEventComponent, {
      width: '1000px', height: '730px',
      data: { networkID: this.id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) window.location.reload();
    });
  }

  openDialogNewNews(): void {
    const dialogRef = this.dialog.open(CreateNewsComponent, {
      width: '1000px',
      data: { networkID: this.id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) window.location.reload();
    });
  }
}
