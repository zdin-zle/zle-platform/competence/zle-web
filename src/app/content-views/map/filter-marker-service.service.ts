import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ClusterMap, NetworkMap, ProfileMap } from 'src/app/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterMarkerService {
  //reference data
  private originalProfiles: ProfileMap[] = [];
  private originalNetworks: NetworkMap[] = [];
  private originalClusters: ClusterMap[] = [];

  private filteredItems        = new BehaviorSubject<(ProfileMap | NetworkMap | ClusterMap)[]>     ([]);
  private activeItems          = new BehaviorSubject<(ProfileMap | NetworkMap | ClusterMap)[]>     ([]);
  private MembersOfActiveItems = new BehaviorSubject<ProfileMap[]>                                 ([]);
  private itemInfoPopover      = new BehaviorSubject<ProfileMap | NetworkMap | ClusterMap | null>  (null);
  private activeTab            = new BehaviorSubject<'profile' | 'network' | 'cluster'>            ('profile');

  // --- Output stuff --- //
  filteredItems$        = this.filteredItems.asObservable();     //items that get filtered by a search string
  activeItems$          = this.activeItems.asObservable();       //items that are activated by its chip or are clicked on and
                                                              // show its members because of that, also the more info popover gets toggled
  MembersOfActiveItems$ = this.MembersOfActiveItems.asObservable(); //members of activeItems -> different colored markers
  itemInfoPopover$      = this.itemInfoPopover.asObservable();   //popover element, that gets toggled on click of a marker or the chip
  activeTab$            = this.activeTab.asObservable();

  // has only IDs
  public get activeItemIDs(): number[] { return this.activeItems  .getValue().map((item) => item.id); }

  // getter for typecasting so that the html template can use these and is not confused by the union type above
  public get profiles$(): Observable<ProfileMap[]> { return this.filteredItems$ as Observable<ProfileMap[]>; }
  public get networks$(): Observable<NetworkMap[]> { return this.filteredItems$ as Observable<NetworkMap[]>; }
  public get clusters$(): Observable<ClusterMap[]> { return this.filteredItems$ as Observable<ClusterMap[]>; }

  // functions to initialize the data from the db
  initProfiles(profiles: ProfileMap[]) { this.originalProfiles = profiles; }
  initNetworks(networks: NetworkMap[]) { this.originalNetworks = networks; }
  initClusters(clusters: ClusterMap[]) { this.originalClusters = clusters; }

  // --- Helper functions --- //

  // curates the info for the popover
  private updateItemInfoPopover(item: number, type: 'profile' | 'network' | 'cluster') {
    let filteredItem: ProfileMap | NetworkMap | ClusterMap | null = null;
    switch (type) {
      case 'profile':
        filteredItem = this.originalProfiles.find((profile) => profile.id === item) || null;
        break;
      case 'network':
        filteredItem = this.originalNetworks.find((network) => network.id === item) || null;
        break;
      case 'cluster':
        filteredItem = this.originalClusters.find((cluster) => cluster.id === item) || null;
        break;
    }
    if (filteredItem) { filteredItem.type = type; }

    this.itemInfoPopover.next(filteredItem);
  }

  private updateMembersOfActiveItems(activeItems: NetworkMap[] | ClusterMap[]) {
    let members: ProfileMap[] = [];
    // iterate through active items and add members to the list
    activeItems.forEach((activeItem, index) => {
      const itemMembers = this.originalProfiles.filter((profile) => activeItem.members.includes(profile.id));

      // add members its distinct color
      itemMembers.forEach(member => member.color = 360 / activeItems.length * index);
      members = members.concat(itemMembers);
    });
    this.MembersOfActiveItems.next(members);
  }

  // --- Public functions --- //

  // set filtered Items, based on searchstring and type
  public filterItems(searchValue: string, type: 'profile' | 'network' | 'cluster') {
    let filteredItems = [];
    switch (type) {
      case 'profile':
        filteredItems = this.originalProfiles.filter(
          (profile) => profile.title.toLowerCase().includes(searchValue) || profile.shortcut.toLowerCase().includes(searchValue)
        );
        filteredItems.forEach((profile) => { profile.color = 0}); // reset color, because MembersOfActiveItems has maybe set it and it only has a reference of the originalProfiles not a deep copy
        break;
      case 'network':
        filteredItems = this.originalNetworks.filter(
          (network) => network.name.toLowerCase().includes(searchValue) || network.shortcut.toLowerCase().includes(searchValue)
        );
        break;
      case 'cluster':
        filteredItems = this.originalClusters.filter(
          (cluster) => cluster.name.toLowerCase().includes(searchValue)
        );
        break;
    }
    filteredItems.forEach((item) => { item.type = type; });
    this.filteredItems.next(filteredItems);
  }

  // updates the active items and its members
  // also triggers the update of the itemInfoPopover
  public updateActiveItems(newActiveItems: number[], type: 'profile' | 'network' | 'cluster') {
    let items = [];
    switch (type) {
      case 'profile':
        items = this.originalProfiles.filter((profile) => newActiveItems.includes(profile.id));
        this.updateMembersOfActiveItems([]);
        break;
      case 'network':
        items = this.originalNetworks.filter((network) => newActiveItems.includes(network.id));
        this.updateMembersOfActiveItems(items);
        break;
      case 'cluster':
        items = this.originalClusters.filter((cluster) => newActiveItems.includes(cluster.id));
        this.updateMembersOfActiveItems(items);
        break;
    }

    // find out first new active item in the list and update ItemInfoPopover
    const [newActiveItemID] = newActiveItems.filter((item: number) => !this.activeItemIDs.includes(item));
    newActiveItemID
      ? this.updateItemInfoPopover(newActiveItemID, type)
      : this.deactivateItemInfoPopover(); //if no new active item is found -> deactivate detail item

    this.activeItems.next(items);
  }
  // toggles an item as active or inactive and toggles ItemInfoPopover
  // or if it is not the type of the active tab, it will only toggle the ItemInfoPopover
  public toggleActiveItem(item: number, type: 'profile' | 'network' | 'cluster') {
    if (this.activeTab.getValue() === type) {
      if (this.activeItemIDs.includes(item)) {
        // remove it from activeItems
        this.updateActiveItems(this.activeItemIDs.filter((activeItem) => activeItem !== item), type);
        return;
      }
      // else add it to activeItems
      this.updateActiveItems([...this.activeItemIDs, item], type);
      return;
    }
    // just toggle the detail item
    this.toggleItemInfoPopover(item, type);
  }

  public deactivateItemInfoPopover() {
    this.itemInfoPopover.next(null);
  }

  public toggleItemInfoPopover(item: number, type: 'profile' | 'network' | 'cluster') {
    const itemInfoPopover = this.itemInfoPopover.getValue();
    if (itemInfoPopover && itemInfoPopover.id === item && itemInfoPopover.type === type) {
      this.deactivateItemInfoPopover();
    } else {
      this.updateItemInfoPopover(item, type);
    }
  }

  // changes tabs and revert back to its saved state
  public tabChange(tab: 'profile' | 'network' | 'cluster', searchValue: string = "", activeItems: number[] = []) {
    this.activeTab.next(tab);
    this.filterItems(searchValue, tab);
    this.updateActiveItems(activeItems, tab);
  }
}
