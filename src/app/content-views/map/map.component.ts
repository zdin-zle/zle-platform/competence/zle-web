import { Component, AfterViewInit, HostListener } from '@angular/core';
import * as L from 'leaflet';

import { Cluster, ClusterMap, NetworkMap, ProfileMap } from 'src/app/models';
import { Global } from 'environments';
import { FilterMarkerService } from './filter-marker-service.service';
import { MatDialog } from '@angular/material/dialog';
import { ClusterDialogComponent } from '../cluster-dialog/cluster-dialog.component';
import { ApiService } from 'src/app/api.service';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});

const iconNetwork = L.icon({
  iconRetinaUrl: 'assets/network_icon.png',
  iconUrl: 'assets/network_icon.png',
  shadowUrl,
  iconSize: [41, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  private map: any;
  mediaEndpoint: string;
  infoItem: ProfileMap | NetworkMap | ClusterMap | null | any = null;

  markers:       (ProfileMap | NetworkMap | ClusterMap)[] = [];
  memberMarkers: (ProfileMap | NetworkMap | ClusterMap)[] = [];

  constructor(
    public fmService: FilterMarkerService,
    private dialog: MatDialog,
    private api: ApiService
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }
  ngAfterViewInit(): void {
    this.initMap();

    this.fmService.filteredItems$.subscribe(markerList => {
      this.markers = markerList;
      this.markerListChange();
    });

    this.fmService.MembersOfActiveItems$.subscribe(markerList => {
      this.memberMarkers = markerList;
      this.markerListChange();
    });

    document.addEventListener('keydown', (event) => {
      if (event.key === 'Escape') this.fmService.deactivateItemInfoPopover();
    });
  }

  //map stuff
  private initMap(): void {
    this.map = L.map('map', {
      center: [52.4162, 10.7359],
      zoom: 7
    });
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);
  }
  private createMarker(markerInfo: any): void {
    if (!markerInfo.address) return;
    if (markerInfo.address.latitude == null || markerInfo.address.longitude == null) return;

    const icon = markerInfo.type == 'network' ? iconNetwork : iconDefault;
    const color = markerInfo.color ? markerInfo.color : 0;

    const marker = L.marker(
      [
        // add a few meters of randomness to avoid overlapping markers
        markerInfo.address.latitude + (Math.random() / 10000),
        markerInfo.address.longitude + (Math.random() / 10000)
      ],
      { icon }
    ).addTo(this.map);

    //@ts-ignore
    marker._icon.style.filter = "contrast(2) hue-rotate(" + color + "deg)";

    // that the toggle Function of the filter component gets triggered by clicking on the marker from the map component
    marker.addEventListener('click', () => {
      this.fmService.toggleActiveItem(markerInfo.id, markerInfo.type);
    });
  }
  private deleteMarkers(): void {
    if (!this.map) return;
    this.map.eachLayer((layer: any) => {
      if (layer instanceof L.Marker) {
        this.map.removeLayer(layer);
      }
    });
  }

  // reset and create new markers every time the list changes
  private markerListChange() {
    this.deleteMarkers();

    const markerList = [...this.markers, ...this.memberMarkers];
    if (markerList.length > 0) {
      markerList.forEach((marker) => {
        this.createMarker(marker);
      });
    }
  }

  isProfileMarker(item: any): item is ProfileMap {
    return item && item.type === 'profile';
  }
  isNetworkMarker(item: any): item is NetworkMap {
    return item && item.type === 'network';
  }
  isClusterMarker(item: any): item is ClusterMap {
    return item && item.type === 'cluster';
  }

  openClusterDialog(cluster: Cluster) {
    this.api.getSingleItem<Cluster>('cluster', cluster.id).subscribe((cluster) => {
      const dialogRef = this.dialog.open(ClusterDialogComponent, { width: '500px' });
      dialogRef.componentInstance.cluster = cluster;
    });
  }
}
