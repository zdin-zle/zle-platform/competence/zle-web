import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent {
  @Input() data: any;
  @Input() mapType: string;
  @Input() mediaEndpoint: string;
  @Output() closeCard = new EventEmitter<void>();
  @Output() openClusterDialog = new EventEmitter<any>();

  isCluster() {
    return this.mapType === 'cluster';
  }

  deactivateDetailCard() {
    this.closeCard.emit();
  }

  handleClick() {
    if (this.isCluster()) {
      this.openClusterDialog.emit(this.data);
    }
  }
}
