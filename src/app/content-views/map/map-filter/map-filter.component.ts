import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ClusterMap, NetworkMap, ProfileMap } from 'src/app/models';
import { forkJoin } from 'rxjs';
import { ApiService } from 'src/app/api.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Global } from 'environments';

import { FilterMarkerService } from '../filter-marker-service.service';
import { MatChipOption } from '@angular/material/chips';

@Component({
  selector: 'app-map-filter',
  templateUrl: './map-filter.component.html',
  styleUrls: ['./map-filter.component.css'],
  animations: [
    trigger('expandAnimation', [
      state('void', style({
        height: '0',
        opacity: '0'
      })),
      state('*', style({
        height: '*',
        opacity: '1'
      })),
      transition('void <=> *', animate('250ms ease-in-out'))
    ])
  ]
})
export class MapFilterComponent implements OnInit {
  mediaEndpoint: string;
  dataLoaded = false;
  expanded = false;

  @ViewChildren('searchFields') searchFields: QueryList<ElementRef>;
  @ViewChildren('chipOption') chipOptions: QueryList<MatChipOption>;

  //search fields
  profileSearchValue: string = '';
  networkSearchValue: string = '';
  clusterSearchValue: string = '';

  //init data
  profiles: ProfileMap[];
  networks: NetworkMap[];
  cluster: ClusterMap[];

  constructor(
    private api: ApiService,
    public fmService: FilterMarkerService
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  ngOnInit(): void {
    this.expanded = window.innerWidth > 800;

    // get data with additional information for output to map
    forkJoin([
      this.api.searchDatabase<ProfileMap[]>('profiles_map'),
      this.api.searchDatabase<NetworkMap[]>('networks_map'),
      this.api.searchDatabase<ClusterMap[]>('clusters_map')
    ]).subscribe(([profilesData, networksData, clustersData]) => {

      this.fmService.initProfiles(profilesData);
      this.fmService.initNetworks(networksData);
      this.fmService.initClusters(clustersData);

      this.dataLoaded = true;
      this.tabChange('profiles'); // init to profiles tab on startup
    });
  }

  ngAfterViewInit(): void {
    // subscribe to fm service to get active marker change and update the chips
    this.fmService.activeItems$.subscribe(items => {
      this.updateChips(items);
    });
  }

  updateChips(items: (ProfileMap | NetworkMap | ClusterMap)[]) {
    // first deselect all chips
    this.chipOptions.forEach(chip => chip.deselect());
    items.forEach(item => {
      // then activate new chips
      const chip = this.chipOptions.find(chip => chip.value === item.id);
      if (chip) chip.select();
    });
  }

  tabChange(tab: string) {
    tab = tab.toLowerCase();

    switch (tab) {
      case 'profiles':
        this.fmService.tabChange('profile', this.profileSearchValue);
        break;
      case 'networks':
        this.fmService.tabChange('network', this.networkSearchValue);
        break;
      case 'cluster':
        this.fmService.tabChange('cluster', this.clusterSearchValue);
        break;
    }
  }

  filterItems(event: Event, type: 'profile' | 'network' | 'cluster') {
    const searchValue = (event.target as HTMLInputElement)?.value;
    this.fmService.filterItems(searchValue, type);
  }

  toggleSettings() {
    this.expanded = !this.expanded;
  }
}
