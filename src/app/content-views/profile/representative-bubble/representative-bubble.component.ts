import { Component, Input, OnInit } from '@angular/core';
import { Global } from 'environments';
import { User } from 'src/app/models';

@Component({
  selector: 'app-representative-bubble',
  templateUrl: './representative-bubble.component.html',
  styleUrls: ['./representative-bubble.component.css']
})
export class RepresentativeBubbleComponent implements OnInit {
  mediaEndpoint: string;
  @Input() representative: User;
  
  photoUrl: string;
  
  
  ngOnInit() {
    this.photoUrl = Global.MEDIA_ENDPOINT + 'representatives/' + this.representative.photo;
  }
}
