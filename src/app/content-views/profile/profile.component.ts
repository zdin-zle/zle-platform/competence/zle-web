import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Global } from 'environments';
import { Utils } from 'src/app/utils';
import { Cluster, Institut, Profile, Publication } from 'src/app/models';
import { switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from 'src/app/api.service';
import { CreateProjectComponent } from 'src/app/forms/dialogs/create-project/create-project.component';
import { CreateLabComponent } from 'src/app/forms/dialogs/create-lab/create-lab.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  @Input() id: number;
  mediaEndpoint: string;

  profile: Profile;
  publications: Publication[];
  publicationsPaged: Publication[];

  public utils = Utils;
  private routeSubscription;

  constructor(
    public http: HttpClient,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public router: Router,
    public sanitizer: DomSanitizer,
    public api: ApiService
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        dialog.closeAll();
        this.router.navigate([event.url]);
      }
    });

    // Subscribe to route params to get the 'id' parameter
    this.routeSubscription = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.loadProfileData(this.id);

    });
  }
  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadProfileData(id: number) {
    //use switchmap to wait for the first request to finish before making the second request to get the publications
    this.api.searchDatabase<Profile>('profile/' + this.id).pipe(
      switchMap(data => {
        this.profile = data;

        // if data.publications is null or is a website link, return empty array
        if (!data.publications) return [];

        //todo: display link correctly without the unnecessary author information
        if (data.publications.startsWith('http') || data.publications.startsWith('www')) return [{records: [{title: data.publications}]}];

        return this.api.searchDatabase('bibfile/' + data.publications);
      })
    ).subscribe((data: any) => {
      this.publications = data.records;
      // Init paginator to the first page
      this.handlePageEvent({ pageIndex: 0, pageSize: 10 });
    });
  }

  getAuthorNames(authors: any[]): string {
    if (Array.isArray(authors) && authors.length > 0) {
      return authors.map(author => {
        return author.name;
      }).join('; ');
    }
    return '-';
  }

  handlePageEvent(event: any) {
    this.publicationsPaged = [];
    if (!this.publications) return;
    for (let i = event.pageIndex * event.pageSize; i < (event.pageIndex + 1) * event.pageSize; i++) {
      if (i < this.publications.length) {
        this.publicationsPaged.push(this.publications[i]);
      }
    }
  }

  openClusterDialog(id: number) {
    this.api.searchDatabase<Cluster>('cluster/' + id).subscribe(data => {
      this.utils.openClusterDialog(this.dialog, data, this.router);
    });
  }

  openProjectDialog(project: any): void {
    const dialogRef = this.dialog.open(CreateProjectComponent, { width: '600px', data: {project: project, profileID: this.id} });
    dialogRef.afterClosed().subscribe(
      (result:any) => {
        if (result) window.location.reload();
      });
  }

  openLabDialog(lab: any): void {
    const dialogRef = this.dialog.open(CreateLabComponent, { width: '600px', data: {lab: lab, profileID: this.id}});
    dialogRef.afterClosed().subscribe(
      (result:any) => {
        if (result) window.location.reload();
      });
  }

  // Type guard to check if institute is of type Institut
  isInstitute(institute: number | Institut | null): institute is Institut {
    // Check that the institute is an object, is not null, and has a shortcut property
    return typeof institute === 'object' && institute !== null && 'shortcut' in institute;
  }
}
