import { Component } from '@angular/core';

@Component({
  selector: 'app-pagenotfound',
  template: `
    <div style="
      height: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      padding-bottom: 54px;
    ">
      <h1>Error 404: We couldn't find the page you were looking for.</h1>
      <button mat-raised-button color="primary" routerLink="/">Go back home</button>
    </div>
  `
})
export class PagenotfoundComponent {}
