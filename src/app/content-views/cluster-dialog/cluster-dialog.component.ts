import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Global } from 'environments';
import { Cluster } from 'src/app/models';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-cluster',
  templateUrl: './cluster-dialog.component.html',
  styleUrls: ['./cluster-dialog.component.css']
})
export class ClusterDialogComponent {
  @Input() cluster: Cluster;

  mediaEndpoint: string;
  constructor(
    public router: Router,
    public http: HttpClient,
    public dialog: MatDialog,
    public api: ApiService,
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }
}
