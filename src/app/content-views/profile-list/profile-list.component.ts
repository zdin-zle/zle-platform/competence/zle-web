import { Component } from '@angular/core'
import { Global } from 'environments';
import { Observable, merge, switchMap } from 'rxjs';
import { Cluster, Institut } from '../../models';
import { ActivatedRoute } from '@angular/router';

import { map } from 'rxjs/operators';
import { ClusterDialogComponent } from 'src/app/content-views/cluster-dialog/cluster-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/api.service';
import { Utils } from 'src/app/utils';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['../main-list-panel/main-list-panel.component.css']
})
export class ProfileListComponent {
  hasNewButton: boolean = false;
  searchText: string = '';
  newButtonText: string = '';
  description: string = '';

  mediaEndpoint: string;
  list$: Observable<any[]>;
  instituteList$: Observable<Institut[]>;

  tempList$: Observable<any[]>;
  institutTempList$: Observable<Institut[]>;

  searchInput: string;

  public utils = Utils;

  constructor(
    public api: ApiService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.hasNewButton = data['hasNewButton'];
      this.searchText = data['searchText'];
      this.newButtonText = data['newButtonText'];
      this.description = data['description'];
    });
    this.list$ = this.api.searchDatabase('profiles');
    this.instituteList$ = this.api.searchDatabase('institutes');
    this.tempList$ = this.list$;
    this.institutTempList$ = this.instituteList$;
  }


  search() {
    const searchInput = this.searchInput?.trim().toLowerCase() || '';

    // Filter profiles based on the search query
    const filteredProfiles$ = this.list$.pipe(
      map(profiles => profiles.filter(profile =>
        profile.title.toLowerCase().includes(searchInput) ||
        profile.shortcut?.toLowerCase().includes(searchInput)
      ))
    );

    // Filter institutes based on the filtered profiles
    this.institutTempList$ = filteredProfiles$.pipe(
      switchMap(filteredProfiles =>
        this.instituteList$.pipe(
          map(institutes =>
            institutes.filter(institute =>
              institute.members.some(member =>
                filteredProfiles.some(profile => profile.id === member)
              )
            )
          )
        )
      )
    );

    this.tempList$ = filteredProfiles$;
  }

  openClusterDialog(cluster: Cluster) {
    const dialogRef = this.dialog.open(ClusterDialogComponent, { width: '500px' });
    dialogRef.componentInstance.cluster = cluster;
  }
}
