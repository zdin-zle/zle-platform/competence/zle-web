import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Global } from 'environments';
import { ApiService } from 'src/app/api.service';
import { Institut, Profile } from 'src/app/models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cluster',
  templateUrl: './institut-dialog.component.html',
  styleUrls: ['./institut-dialog.component.css']
})
export class InstitutDialogComponent {
  @Input() institut: Institut;
  @Input() allProfiles$: Observable<Profile[]>;

  mediaEndpoint: string;
  constructor(
    public router: Router,
    public http: HttpClient,
    public dialog: MatDialog,
    public api: ApiService
  ) {
    this.mediaEndpoint = Global.MEDIA_ENDPOINT;
  }

  deleteInstitute() {
    this.api.deleteEntry('institute', this.institut.id, 'Institution', true)
  }

  closeDialog() {
    this.dialog.closeAll();
  }
}

