import { Cluster, Institut } from './models';
import { MatDialog } from '@angular/material/dialog';
import { ClusterDialogComponent } from './content-views/cluster-dialog/cluster-dialog.component';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { InstitutDialogComponent } from './content-views/profile-list/institut-dialog/institut-dialog.component';
import { ApiService } from './api.service';

export class Utils {

    public static openClusterDialog(dialog: MatDialog, cluster: Cluster, router: Router) {
      const dialogRef = dialog.open(ClusterDialogComponent, { width: '500px' });
      dialogRef.componentInstance.cluster = cluster;

      dialogRef.afterClosed().subscribe((url: any[]) => {
        router.navigate(url);
      });
    }

    public static sanitizeExternalLink(userInputLink: string, sanitizer: DomSanitizer): SafeUrl | null {
      let trimmedLink = userInputLink.trim();

      // Füge "https://" hinzu, wenn kein Protokoll angegeben ist
      if (!/^https?:\/\//i.test(trimmedLink)) {
        trimmedLink = `https://${trimmedLink}`;
      }

      // Füge "/" am Ende hinzu, wenn es nicht bereits vorhanden ist
      if (!trimmedLink.endsWith("/")) {
        trimmedLink += "/";
      }

      // Sichere die URL und gib sie zurück
      return sanitizer.bypassSecurityTrustUrl(trimmedLink);
    }

    public static openInstituteDialog(institute: Institut, dialog: MatDialog, api: ApiService) {

      const dialogRef = dialog.open(InstitutDialogComponent, { width: '500px' });
      dialogRef.componentInstance.institut = institute;
      dialogRef.componentInstance.allProfiles$ = api.searchDatabase('profiles');

      //todo: get list from parent component and not fetch it again
    }
  }
