
import { LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//3rd party
import { EllipsisModule } from 'ngx-ellipsis';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

//material
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatRippleModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';

//date picker things
import { DatePipe } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
registerLocaleData(localeDe);

//routes
import { HomeComponent } from './content-views/home/home.component';
import { NetworkComponent } from './content-views/network/network.component';
import { ProfileComponent } from './content-views/profile/profile.component';
import { ClusterDialogComponent } from './content-views/cluster-dialog/cluster-dialog.component';
import { InstitutDialogComponent } from './content-views/profile-list/institut-dialog/institut-dialog.component';
import { RepresentativeBubbleComponent } from './content-views/profile/representative-bubble/representative-bubble.component';
import { MapComponent } from './content-views/map/map.component';
import { MapFilterComponent } from './content-views/map/map-filter/map-filter.component';

//components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/navigation/header/header.component';
import { SubheaderComponent } from './components/navigation/subheader/subheader.component';
import { FooterComponent } from './components/navigation/footer/footer.component';
import { NewsCardComponent } from './components/cards/news-card/news-card.component';
import { EventCardComponent } from './components/cards/event-card/event-card.component';
import { TitleLogoCardComponent } from './components/cards/title-logo-card/title-logo-card.component';
import { ProjectCardComponent } from './components/cards/project-card/project-card.component';
import { ClusterCardComponent } from './components/cards/cluster-card/cluster-card.component';
import { MainListPanelComponent } from './content-views/main-list-panel/main-list-panel.component';
import { ItemCardComponent } from './components/cards/item-card/item-card.component';
import { EventCardDialogComponent } from './components/cards/event-card/event-card-dialog/event-card-dialog.component';
import { AddressComponent } from './components/address/address.component';
import { CloseDialogComponent } from './components/close-dialog.component';

//forms
import { ProfileFormComponent } from './forms/profile-form/profile-form.component';
import { UploadBoxComponent } from './forms/upload-box/upload-box.component';
import { DragoverDirective } from './forms/dragover.directive';
import { CreateProjectComponent } from './forms/dialogs/create-project/create-project.component';
import { CreateClusterComponent } from './forms/dialogs/create-cluster/create-cluster.component';
import { CreateLabComponent } from './forms/dialogs/create-lab/create-lab.component';
import { NetworkFormComponent } from './forms/network-form/network-form.component';
import { ClusterFormComponent } from './forms/cluster-form/cluster-form.component';
import { SelectAndFilterComponent } from './components/select-and-filter/select-and-filter.component';
import { CreateNewsComponent } from './forms/dialogs/create-news/create-news.component';
import { CreateEventComponent } from './forms/dialogs/create-event/create-event.component';

//pipes
import { NewlinePipe } from './pipes/newline.pipe';
import { NewlineBulletPipe } from './pipes/newline-bullet.pipe';
import { PagenotfoundComponent } from './content-views/pagenotfound.component';
import { InfoCardComponent } from './content-views/map/info-card/info-card.component';
import { ProfileListComponent } from './content-views/profile-list/profile-list.component';
import { CreateInstitutComponent } from './forms/dialogs/create-institut/create-institut.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SubheaderComponent,
    NetworkComponent,
    NewsCardComponent,
    EventCardComponent,
    TitleLogoCardComponent,
    ProfileComponent,
    ProjectCardComponent,
    ClusterCardComponent,
    ClusterDialogComponent,
    InstitutDialogComponent,
    ProfileFormComponent,
    UploadBoxComponent,
    DragoverDirective,
    CreateProjectComponent,
    CreateClusterComponent,
    NewlinePipe,
    NewlineBulletPipe,
    CreateLabComponent,
    RepresentativeBubbleComponent,
    MapComponent,
    MapFilterComponent,
    MainListPanelComponent,
    ItemCardComponent,
    EventCardDialogComponent,
    NetworkFormComponent,
    ClusterFormComponent,
    SelectAndFilterComponent,
    CreateNewsComponent,
    CreateEventComponent,
    AddressComponent,
    CloseDialogComponent,
    PagenotfoundComponent,
    InfoCardComponent,
    ProfileListComponent,
    CreateInstitutComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatMenuModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    NgbModule,
    MatExpansionModule,
    MatSelectModule,
    MatListModule,
    MatChipsModule,
    MatDialogModule,
    EllipsisModule,
    LeafletModule,
    MatTabsModule,
    MatTableModule,
    MatRippleModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline' } },

    //necessary to change the date format in the datepicker, propably bunch of unnesserary stuff
    { provide: MAT_DATE_LOCALE, useValue: 'de-DE' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: LOCALE_ID, useValue: 'de-DE' },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
