//Install express server
const express = require('express');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory
app.use('/competence', express.static('./dist/zle-web'));

app.get('/competence/*', (req, res) =>
    res.sendFile('index.html', { root: 'dist/zle-web/' }),
);

// Start the app by listening to port 80
app.listen(process.env.PORT || 80);