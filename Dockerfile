# build stage
FROM node:18.10-alpine as node
WORKDIR /app
COPY package*.json ./
# RUN npm config set user 0
# RUN npm config set unsafe-perm true
# RUN npm install
COPY . .
RUN npm ci
# ENV NODE_ENV production
RUN npm run ng build -- --configuration=development
# RUN npm prune

