## ZleWeb

The frontend for the ZLE Competence application. In the competence platform a user can create a profile, network and cluster to manage and see comeptence fields in a given research area. With the help of the ZLE Competence application, a user can find others with the same or similar competence fields.

To run it, you need to have the backend running as well. The backend can be found [here](https://gitlab.com/zdin-zle/zle-platform/competence/zle-api). This project is written in Angular version 15.1.5 and uses the Angular Material Design Framework.

## Documentation 

[see here](https://dev-zle.offis.de/competence/docs/) for more information.

## Local installation of ZLE-WEB Angular Frontend with ng serve for live editing

1. clone Repo
2. install nodejs bundled with npm -> https://nodejs.org/en/download/package-manager
3. execute:
```bash
npm install #should install everything needed
ng serve
```
or for publishing it in local network to test with other devices like mobile phones
```bash
ng serve --host 0.0.0.0
```


## deploy docker container locally (for testing purposes)
```bash
docker build -t competence_web_image .
docker compose up -d
```
    
you should be able to acces frontend under
http://localhost:8080/competence/

## deploy docker container on vm remotely

1. commit and push changes to gitlab
2. ssh into vm
3. clone repo, or pull changes
4. get super user acces

        sudo su

5. go into zle-web folder and build docker image

        docker build -t competence_web_image -f Dockerfile.production .

6. compose image with

        docker compose -f /home/dev/competence/competence_web/docker-compose.prod.yml  up -d
